package jp.co.tomokoru.bravesharing.service;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {

    private static Retrofit retrofit;

    private static final String url = "http://spajam.ap-northeast-1.elasticbeanstalk.com/";

    static {
        retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static <T> T createService(Class<T> clz) {
        return retrofit.create(clz);
    }
}
