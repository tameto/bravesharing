package jp.co.tomokoru.bravesharing;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import java.util.HashMap;
import java.util.Map;

public class MyPageActivity extends AppCompatActivity {

    public static final int INTENT_CHOOSE_AVATAR = 12;
    private final Map<String, Integer> drawableMap = new HashMap<String, Integer>() {{
        put("1", R.drawable.avatar1);
        put("2", R.drawable.avatar2);
        put("3", R.drawable.avatar3);
        put("4", R.drawable.avatar4);
        put("5", R.drawable.avatar5);
        put("6", R.drawable.avatar6);
    }};

    private ImageView mAvatar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_page);

        mAvatar = (ImageView) findViewById(R.id.avatar);

        SharedPreferences prefer = getSharedPreferences("devil", MODE_PRIVATE);
        String avatarId = prefer.getString("avatarId", "1");

        mAvatar = (ImageView) findViewById(R.id.avatar);
        mAvatar.setImageResource(drawableMap.get(avatarId));
    }

    public void onClickChooseAvatar(View view) {
        startActivityForResult(new Intent(this, ChooseAvatarActivity.class), INTENT_CHOOSE_AVATAR);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == INTENT_CHOOSE_AVATAR && resultCode == RESULT_OK) {
            String avatarId = data.getStringExtra("avatarId");

            SharedPreferences prefer = getSharedPreferences("devil", MODE_PRIVATE);
            SharedPreferences.Editor editor = prefer.edit();
            editor.putString("avatarId", avatarId);
            editor.apply();

            mAvatar.setImageResource(drawableMap.get(avatarId));
        }
    }
}
