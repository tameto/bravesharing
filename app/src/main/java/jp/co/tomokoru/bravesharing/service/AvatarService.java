package jp.co.tomokoru.bravesharing.service;

import jp.co.tomokoru.bravesharing.model.Avatar;
import jp.co.tomokoru.bravesharing.model.User;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

import java.util.List;
import java.util.Map;

public interface AvatarService {

    @GET("api/position/list")
    Call<List<User>> getSenderData();

    @FormUrlEncoded
    @POST("api/position/register")
    Call<ResponseBody> positionRegister(@Field("position_x") String position_x, @Field("position_y") String position_y, @Field("position_z") String position_z, @Field("avatar_id") String avatar_id);

    @FormUrlEncoded
    @POST("api/user/avatar")
    Call<ResponseBody> uploadAvatar(@Field("avatar_id") final String avatarId);

    @GET("api/senderposition/list")
    Call<List<User>> getReceiverData();

    @FormUrlEncoded
    @POST("api/senderposition/register")
    Call<ResponseBody> sendPosition(@Field("position_x") String position_x, @Field("position_y") String position_y, @Field("position_z") String position_z, @Field("avatar_id") String avatar_id);

    @GET("api/item/list")
    Call<List<User>> getItemList();

    @FormUrlEncoded
    @POST("api/item/register")
    Call<ResponseBody> sendItem(@Field("position_x") String position_x, @Field("position_y") String position_y, @Field("position_z") String position_z, @Field("avatar_id") String avatar_id);
}
