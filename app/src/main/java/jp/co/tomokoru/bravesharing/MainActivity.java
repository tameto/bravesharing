package jp.co.tomokoru.bravesharing;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareMediaContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.widget.ShareDialog;
import com.onesignal.OSNotification;
import com.onesignal.OneSignal;
import jp.co.tomokoru.bravesharing.ar.PointToPointActivity;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements OneSignal.NotificationReceivedHandler {

    private static final String TAG = MainActivity.class.getSimpleName();

    private Activity mActivity;
    private CallbackManager mCallbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mActivity = this;
        mCallbackManager = CallbackManager.Factory.create();

        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(final LoginResult loginResult) {
                SharePhoto photo = new SharePhoto.Builder()
                        .setBitmap(BitmapFactory.decodeResource(mActivity.getResources(), android.R.drawable.stat_sys_warning))
                        .build();
                ShareContent shareContent = new ShareMediaContent.Builder()
                        .addMedium(photo)
                        .setShareHashtag(new ShareHashtag.Builder().setHashtag("#sharing-yuuki").build())
                        .build();
                ShareDialog shareDialog = new ShareDialog(mActivity);
                shareDialog.show(shareContent, ShareDialog.Mode.AUTOMATIC);
            }

            @Override
            public void onCancel() {
                Log.v(TAG, "onCancel");
            }

            @Override
            public void onError(final FacebookException error) {
                Log.v(TAG, "onError");
            }
        });
    }

    public void onClickSetting(View view) {
        startActivity(new Intent(this, MyPageActivity.class));
    }

    public void onClickSender(View view) {
        Intent intent = new Intent(this, PointToPointActivity.class);
        intent.putExtra("UserType", "sender");

        startActivity(intent);
    }

    public void onClickReceiver(View view) {
        SharedPreferences prefer = getSharedPreferences("devil", MODE_PRIVATE);
        String ssoMessage = prefer.getString("sos_message", "ほんのちょっとすつだけみんなの勇気を分けてくれ！");

        final EditText input = new EditText(this);
        input.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        input.setText(ssoMessage);

        AlertDialog dialog = new AlertDialog.Builder(this)
                .setView(input)
                .setTitle("SOSメッセージ")
                .setMessage("SOS送信時のメッセージを設定して下さい")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String ssoMessage = input.getText().toString();

                        SharedPreferences prefer = getSharedPreferences("devil", MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefer.edit();
                        editor.putString("sos_message", ssoMessage);
                        editor.apply();

                        push(ssoMessage);
                    }
                })
                .setNegativeButton("キャンセル", null)
                .create();

        dialog.show();
    }

    private void push(String message) {
        //            String playerId = "8fcccc40-645d-4b43-b858-23d29b6fce3a"; // Fujitsu
        String playerId = "7ae9c689-d117-4595-98e5-f8e5fef5bdf7"; // Pixel

        Map<String, String> contents = new HashMap<>();
        contents.put("ja", message);
        contents.put("en", message);

        Map map = new HashMap<>();
        map.put("contents", contents);
        map.put("include_player_ids", Arrays.asList(playerId));

        OneSignal.postNotification(new JSONObject(map), null);

        new AlertDialog.Builder(this)
                .setTitle("Facebook投稿")
                .setMessage("あなたのSOSをFacebookにも投稿しますか？")
                .setPositiveButton("はい", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        LoginManager.getInstance().logInWithReadPermissions(mActivity, Arrays.asList("email public_profile"));
                    }
                })
                .setNegativeButton("いいえ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        Intent intent = new Intent(mActivity, PointToPointActivity.class);
                        intent.putExtra("UserType", "receiver");
                        mActivity.startActivity(intent);
                    }
                })
                .create()
                .show();
    }

    @Override
    public void notificationReceived(OSNotification notification) {
        JSONObject data = notification.payload.additionalData;
        if (data != null) {
            String customKey = data.optString("customkey", null);
            if (customKey != null) {
                Log.i("OneSignalExample", "customkey set with value: " + customKey);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.v(TAG, "requestCode = " + String.valueOf(requestCode) + ", resultCode = " + String.valueOf(resultCode));

        mCallbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 64207 && resultCode == RESULT_OK) {
            Intent intent = new Intent(this, PointToPointActivity.class);
            intent.putExtra("UserType", "receiver");

            startActivity(intent);
        }
    }

}
