/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.co.tomokoru.bravesharing.ar;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.hardware.display.DisplayManager;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;
import com.google.atap.tango.mesh.TangoMesh;
import com.google.atap.tangoservice.Tango;
import com.google.atap.tangoservice.Tango.OnTangoUpdateListener;
import com.google.atap.tangoservice.TangoCameraIntrinsics;
import com.google.atap.tangoservice.TangoConfig;
import com.google.atap.tangoservice.TangoCoordinateFramePair;
import com.google.atap.tangoservice.TangoErrorException;
import com.google.atap.tangoservice.TangoEvent;
import com.google.atap.tangoservice.TangoException;
import com.google.atap.tangoservice.TangoInvalidException;
import com.google.atap.tangoservice.TangoOutOfDateException;
import com.google.atap.tangoservice.TangoPointCloudData;
import com.google.atap.tangoservice.TangoPoseData;
import com.google.atap.tangoservice.TangoXyzIjData;
import com.google.atap.tangoservice.experimental.TangoImageBuffer;
import com.projecttango.tangosupport.TangoPointCloudManager;
import com.projecttango.tangosupport.TangoSupport;
import jp.co.tomokoru.bravesharing.R;
import jp.co.tomokoru.bravesharing.meshing.TangoMesher;
import jp.co.tomokoru.bravesharing.model.User;
import jp.co.tomokoru.bravesharing.service.AvatarService;
import jp.co.tomokoru.bravesharing.service.ServiceGenerator;
import okhttp3.ResponseBody;
import org.rajawali3d.math.vector.Vector3;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.lang.Float.*;

public class PointToPointActivity extends Activity implements View.OnTouchListener {
    private class MeasuredPoint {
        public double mTimestamp;
        public float[] mDepthTPoint;

        public MeasuredPoint(double timestamp, float[] depthTPoint) {
            mTimestamp = timestamp;
            mDepthTPoint = depthTPoint;
        }
    }

    private static final String TAG = PointToPointActivity.class.getSimpleName();

    private static final String CAMERA_PERMISSION = Manifest.permission.CAMERA;
    private static final int CAMERA_PERMISSION_CODE = 0;

    private static final int UPDATE_UI_INTERVAL_MS = 100;

    private static final int INVALID_TEXTURE_ID = 0;

    //private SurfaceView mSurfaceView;
    private PointToPointRenderer mPointRenderer;

    private TangoPointCloudManager mPointCloudManager;
    private Tango mTango;
    private TangoConfig mConfig;
    private boolean mIsConnected = false;
    private boolean mIsGlRenderer = false;
    private double mCameraPoseTimestamp = 0;
    private volatile TangoImageBuffer mCurrentImageBuffer;

    private int mConnectedTextureIdGlThread = INVALID_TEXTURE_ID;
    private AtomicBoolean mIsFrameAvailableTangoThread = new AtomicBoolean(false);
    private double mRgbTimestampGlThread1;


    private int mPointSwitch = 0;

    private MeasuredPoint[] mMeasuredPoints = new MeasuredPoint[4];

    private Stack<Vector3> mMeasurePoitnsInOpenGLSpace = new Stack<Vector3>();
    private Stack<Vector3> mMeasurePoitnsInOpenGLSpaceSender = new Stack<Vector3>();

    // Handles the debug text UI update loop.
    private Handler mHandler = new Handler();

    private int mDisplayRotation = 0;

    private GLSurfaceView mSurfaceView;
    private OcclusionRenderer mRenderer;
    private TangoMesher mTangoMesher;
    private volatile TangoMesh[] mMeshVector;
    // ユーザータイプ
    public String UserType = "sender";
    public int mAvatarCount = 0;


    private float[] mDepthTPlane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_point_to_point);

        UserType = getIntent().getStringExtra("UserType");
        Log.d("UserType",UserType);
        //mPointRenderer = new PointToPointRenderer(this);
        mSurfaceView = (GLSurfaceView) findViewById(R.id.ar_view);
        mSurfaceView.setZOrderOnTop(false);
        mSurfaceView.setOnTouchListener(this);

        mPointCloudManager = new TangoPointCloudManager();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("はじめに");
        builder.setMessage("中心位置を決めて下さい!");
        builder.setPositiveButton("OK", null);
        AlertDialog dialog = builder.create();
        dialog.show();

        // 自身の中心点
        mMeasuredPoints[0] = null;
        // 自身の位置
        mMeasuredPoints[1] = null;
        // 相手の中心点
        mMeasuredPoints[2] = null;
        // 相手の位置
        mMeasuredPoints[3] = null;

        DisplayManager displayManager = (DisplayManager) getSystemService(DISPLAY_SERVICE);
        if (displayManager != null) {
            displayManager.registerDisplayListener(new DisplayManager.DisplayListener() {
                @Override
                public void onDisplayAdded(int displayId) {
                }

                @Override
                public void onDisplayChanged(int displayId) {
                    synchronized (this) {
                        setDisplayRotation();
                    }
                }

                @Override
                public void onDisplayRemoved(int displayId) {
                }
            }, null);
        }
        connectRenderer();
    }

    @Override
    protected void onStart() {
        super.onStart();

        mSurfaceView.onResume();

        mSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);

        if (checkAndRequestPermissions()) {
            bindTangoService();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        clearLine();
        //        synchronized (this) {
        //            try {
        //if (mIsGlRenderer) {
            //mPointRenderer.getCurrentScene().clearFrameCallbacks();
        //}
        //                if (mTango != null) {
        //                    //mTango.disconnectCamera(TangoCameraIntrinsics.TANGO_CAMERA_COLOR);        //消していいのかな
        //                    mTango.disconnect();
        //                }
        //                mConnectedTextureIdGlThread = INVALID_TEXTURE_ID;
        //                mIsConnected = false;
        //            } catch (TangoErrorException e) {
        //                Log.e(TAG, getString(R.string.exception_tango_error), e);
        //            }
        //        }

        mSurfaceView.onPause();
        synchronized (this) {
            try {
                if (mTangoMesher != null) {
                    mTangoMesher.stopSceneReconstruction();
                    mTangoMesher.resetSceneReconstruction();
                    mTangoMesher.release();
                }
                if (mTango != null) {
                    mTango.disconnect();
                }
                mConnectedTextureIdGlThread = INVALID_TEXTURE_ID;
                mIsConnected = false;
            } catch (TangoErrorException e) {
                Log.e(TAG, getString(R.string.exception_tango_error), e);
            }
        }

    }

    /**
     * Initialize Tango Service as a normal Android Service.
     */
    private void bindTangoService() {
        if (!mIsConnected) {
            mTango = new Tango(PointToPointActivity.this, new Runnable() {
                // Pass in a Runnable to be called from UI thread when Tango is ready; this Runnable
                // will be running on a new thread.？
                // When Tango is ready, we can call Tango functions safely here only when there are no
                // UI thread changes involved.
                @Override
                public void run() {
                    synchronized (PointToPointActivity.this) {
                        try {
                            mConfig = setupTangoConfig(mTango);
                            mTango.connect(mConfig);
                            TangoSupport.initialize(mTango);
                            startupTango();
                            //connectRenderer();
                            mIsConnected = true;
                            setDisplayRotation();
                        } catch (TangoOutOfDateException e) {
                            Log.e(TAG, getString(R.string.exception_out_of_date), e);
                            showsToastAndFinishOnUiThread(R.string.exception_out_of_date);
                        } catch (TangoErrorException e) {
                            Log.e(TAG, getString(R.string.exception_tango_error), e);
                            showsToastAndFinishOnUiThread(R.string.exception_tango_error);
                        } catch (TangoInvalidException e) {
                            Log.e(TAG, getString(R.string.exception_tango_invalid), e);
                            showsToastAndFinishOnUiThread(R.string.exception_tango_invalid);
                        }
                    }
                }
            });
        }

    }

    private TangoConfig setupTangoConfig(Tango tango) {
        TangoConfig config = tango.getConfig(TangoConfig.CONFIG_TYPE_DEFAULT);
        config.putBoolean(TangoConfig.KEY_BOOLEAN_LOWLATENCYIMUINTEGRATION, true);
        config.putBoolean(TangoConfig.KEY_BOOLEAN_COLORCAMERA, true);
        config.putBoolean(TangoConfig.KEY_BOOLEAN_DEPTH, true);
        config.putInt(TangoConfig.KEY_INT_DEPTH_MODE, TangoConfig.TANGO_DEPTH_MODE_POINT_CLOUD);
        config.putBoolean(TangoConfig.KEY_BOOLEAN_DRIFT_CORRECTION, true);

        return config;
    }

    private void startupTango() {
        List<TangoCoordinateFramePair> framePairs = new ArrayList<TangoCoordinateFramePair>();

        mTango.connectListener(framePairs, new OnTangoUpdateListener() {
            @Override
            public void onPoseAvailable(TangoPoseData pose) {
                // We are not using OnPoseAvailable for this app.
            }

            //            @Override
            //            public void onFrameAvailable(int cameraId) {
            //                if (cameraId == TangoCameraIntrinsics.TANGO_CAMERA_COLOR) {
            //                    // Mark a camera frame as available for rendering in the OpenGL thread.
            //                    mIsFrameAvailableTangoThread.set(true);
            //                    mSurfaceView.requestRender();
            //                }
            //            }

            @Override
            public void onFrameAvailable(int cameraId) {
                if (cameraId == TangoCameraIntrinsics.TANGO_CAMERA_COLOR) {
                    if (mSurfaceView.getRenderMode() != GLSurfaceView.RENDERMODE_WHEN_DIRTY) {
                        mSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
                    }
                    mIsFrameAvailableTangoThread.set(true);
                    mSurfaceView.requestRender();
                }
            }

            @Override
            public void onXyzIjAvailable(TangoXyzIjData xyzIj) {
            }

            //            @Override
            //            public void onPointCloudAvailable(TangoPointCloudData pointCloud) {
            //                mPointCloudManager.updatePointCloud(pointCloud);
            //            }

            @Override
            public void onPointCloudAvailable(TangoPointCloudData tangoPointCloudData) {
                if (mTangoMesher != null) {
                    mTangoMesher.onPointCloudAvailable(tangoPointCloudData);
                }
                if (mPointCloudManager != null) {
                    mPointCloudManager.updatePointCloud(tangoPointCloudData);
                }
            }

            @Override
            public void onTangoEvent(TangoEvent event) {
                // We are not using OnPoseAvailable for this app.
            }
        });

        mTangoMesher = new TangoMesher(new TangoMesher.OnTangoMeshesAvailableListener() {
            @Override
            public void onMeshesAvailable(TangoMesh[] tangoMeshes) {
                mMeshVector = tangoMeshes;
            }
        });

        mTangoMesher.setColorCameraCalibration(mTango.getCameraIntrinsics(TangoCameraIntrinsics
                .TANGO_CAMERA_COLOR));
        mTangoMesher.setDepthCameraCalibration(mTango.getCameraIntrinsics(TangoCameraIntrinsics
                .TANGO_CAMERA_DEPTH));
        mTangoMesher.startSceneReconstruction();

        mTango.experimentalConnectOnFrameListener(TangoCameraIntrinsics.TANGO_CAMERA_COLOR,
                new Tango.OnFrameAvailableListener() {
                    @Override
                    public void onFrameAvailable(TangoImageBuffer tangoImageBuffer, int i) {
                        mCurrentImageBuffer = copyImageBuffer(tangoImageBuffer);
                    }

                    TangoImageBuffer copyImageBuffer(TangoImageBuffer imageBuffer) {
                        ByteBuffer clone = ByteBuffer.allocateDirect(imageBuffer.data.capacity());
                        imageBuffer.data.rewind();
                        clone.put(imageBuffer.data);
                        imageBuffer.data.rewind();
                        clone.flip();
                        return new TangoImageBuffer(imageBuffer.width, imageBuffer.height,
                                imageBuffer.stride, imageBuffer.frameNumber,
                                imageBuffer.timestamp, imageBuffer.format, clone);
                    }
                });
    }


    private void connectRenderer() {
        mSurfaceView.setEGLContextClientVersion(2);
        mRenderer = new OcclusionRenderer(PointToPointActivity.this, new OcclusionRenderer
                .RenderCallback() {
            @Override
            public void preRender() {
                // NOTE: This is called from the OpenGL render thread, after all the renderer
                // onRender callbacks have a chance to run and before scene objects are rendered
                // into the scene.

                try {
                    // Synchronize against disconnecting while using the service.
                    synchronized (PointToPointActivity.this) {
                        // Don't execute any tango API actions if we're not connected to the
                        // service.
                        if (!mIsConnected) {
                            return;
                        }

                        // Set up scene camera projection to match RGB camera intrinsics.
                        if (!mRenderer.isProjectionMatrixConfigured()) {
                            // Set up scene camera projection to match RGB camera intrinsics.
                            TangoCameraIntrinsics intrinsics =
                                    TangoSupport.getCameraIntrinsicsBasedOnDisplayRotation(
                                            TangoCameraIntrinsics.TANGO_CAMERA_COLOR,
                                            mDisplayRotation);
                            mRenderer.setProjectionMatrix(
                                    projectionMatrixFromCameraIntrinsics(intrinsics), 0.1f, 100f);
                        }
                        // Connect the Tango SDK to the OpenGL texture ID where we are
                        // going to render the camera.
                        // NOTE: This must be done after both the texture is generated
                        // and the Tango Service is connected.
                        if (mConnectedTextureIdGlThread != mRenderer.getTextureId()) {
                            mTango.connectTextureId(TangoCameraIntrinsics.TANGO_CAMERA_COLOR,
                                    mRenderer.getTextureId());
                            mConnectedTextureIdGlThread = mRenderer.getTextureId();
                            Log.d(TAG, "connected to texture id: " + mRenderer.getTextureId());
                        }
                        // If there is a new RGB camera frame available, update the texture and
                        // scene camera pose.
                        if (mIsFrameAvailableTangoThread.compareAndSet(true, false)) {
                            mRgbTimestampGlThread1 =
                                    mTango.updateTexture(TangoCameraIntrinsics.
                                            TANGO_CAMERA_COLOR);
                            // Calculate the camera color pose at the camera frame update time in
                            // OpenGL engine.
                            TangoSupport.TangoMatrixTransformData ssTrgb =
                                    TangoSupport.getMatrixTransformAtTime(
                                            mRgbTimestampGlThread1,
                                            TangoPoseData.COORDINATE_FRAME_START_OF_SERVICE,
                                            TangoPoseData.COORDINATE_FRAME_CAMERA_COLOR,
                                            TangoSupport.TANGO_SUPPORT_ENGINE_OPENGL,
                                            TangoSupport.TANGO_SUPPORT_ENGINE_OPENGL,
                                            mDisplayRotation);

                            if (ssTrgb.statusCode == TangoPoseData.POSE_VALID) {
                                // Update the camera pose from the renderer.
                                mRenderer.updateViewMatrix(ssTrgb.matrix);
                            } else {
                                Log.w(TAG, "Can't get last camera pose");
                            }
                        }

                    }
                    // Update mesh.
                    updateMeshMap();

                    // Avoid crashing the application due to unhandled exceptions.
                } catch (TangoErrorException e) {
                    Log.e(TAG, "Tango API call error within the OpenGL render thread", e);
                } catch (TangoInvalidException e) {
                    Log.e(TAG, "Tango API call error within the OpenGL render thread", e);
                }
            }
        });
        mSurfaceView.setRenderer(mRenderer);
    }


    /**
     * Use Tango camera intrinsics to calculate the projection Matrix for the Rajawali scene.
     */
    private static float[] projectionMatrixFromCameraIntrinsics(TangoCameraIntrinsics intrinsics) {
        // Uses frustumM to create a projection matrix taking into account calibrated camera
        // intrinsic parameter.
        // Reference: http://ksimek.github.io/2013/06/03/calibrated_cameras_in_opengl/
        float near = 0.1f;
        float far = 100;

        double cx = intrinsics.cx;
        double cy = intrinsics.cy;
        double width = intrinsics.width;
        double height = intrinsics.height;
        double fx = intrinsics.fx;
        double fy = intrinsics.fy;

        double xscale = near / fx;
        double yscale = near / fy;

        double xoffset = (cx - (width / 2.0)) * xscale;
        // Color camera's coordinates has y pointing downwards so we negate this term.
        double yoffset = -(cy - (height / 2.0)) * yscale;

        float m[] = new float[16];
        Matrix.frustumM(m, 0,
                (float) (xscale * -width / 2.0 - xoffset),
                (float) (xscale * width / 2.0 - xoffset),
                (float) (yscale * -height / 2.0 - yoffset),
                (float) (yscale * height / 2.0 - yoffset), near, far);
        return m;
    }

    private int touchCounter = 0;

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        final AvatarService service = ServiceGenerator.createService(AvatarService.class);
        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {

            float u = motionEvent.getX() / view.getWidth();
            float v = motionEvent.getY() / view.getHeight();

            // 中心位置を決定
            if (touchCounter == 0) {
                try {
                    MeasuredPoint newMeasuredPoint;
                    synchronized (this) {
                        // 中央

                        //if(mRgbTimestampGlThread1 != 0) {
                            mDepthTPlane = doFitPlane(u, v, mRgbTimestampGlThread1);
                            newMeasuredPoint = getDepthAtTouchPosition(u, v);
                            if (newMeasuredPoint != null) {
                                updateLine(newMeasuredPoint);
                            } else {
                                Log.w(TAG, "Point was null.");
                            }

                        //}
                    }


                } catch (TangoException t) {
                    Toast.makeText(getApplicationContext(),
                            R.string.failed_measurement,
                            Toast.LENGTH_SHORT).show();
                    Log.e(TAG, getString(R.string.failed_measurement), t);
                } catch (SecurityException t) {
                    Toast.makeText(getApplicationContext(),
                            R.string.failed_permissions,
                            Toast.LENGTH_SHORT).show();
                    Log.e(TAG, getString(R.string.failed_permissions), t);
                }

                // 画像の表示
            } else {

                final float[] planeFitTransform1;
                //                float[] planeFitTransform2;
                //                float[] planeFitTransform3;
                //                float[] planeFitTransform4;
                //                float[] planeFitTransform5;
                // 送る側は相手のデフォルトアバターしか見れない
                if (UserType.equals("sender")) {

                    MeasuredPoint newMeasuredPoint;
                    synchronized (this) {
                        if(mRgbTimestampGlThread1 != 0) {
                            planeFitTransform1 = doFitPlane(u, v, mRgbTimestampGlThread1);
                            if (planeFitTransform1 != null) {
                                // Place the earth 30 cm above the plane.
                                if(mAvatarCount < 4) {

                                    Matrix.translateM(planeFitTransform1, 0, 0, 0, 0.3f);
                                    mRenderer.updateEarthTransform(planeFitTransform1, touchCounter % 4 + 1 , 1);
                                    service.sendItem(String.valueOf(u), String.valueOf(v), String.valueOf(0.3f), String.valueOf(1)).enqueue(new Callback<ResponseBody>() {
                                        @Override
                                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                        }

                                        @Override
                                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                                            Log.v(TAG, "onFailure   ////  " + t.getMessage());
                                        }
                                    });

                                    mAvatarCount++;
                                }
                                if(mRgbTimestampGlThread1 != 0) {
                                    mDepthTPlane = doFitPlane(u, v, mRgbTimestampGlThread1);
                                    newMeasuredPoint = getDepthAtTouchPosition(u, v);
                                    updateLine(newMeasuredPoint);
                                }
                            }
                        }
                    }
                } else {
                    // レシーバー
                    MeasuredPoint newMeasuredPoint;
                    synchronized (this) {
                        // 相手アバター
                        planeFitTransform1 = doFitPlane(u, v, mRgbTimestampGlThread1);
                        //                        // アイテムアバター1
                        //                        planeFitTransform2 = doFitPlane((float)0.5298711, (float)0.5612021, mRgbTimestampGlThread2);
                        //                        // アイテムアバター2
                        //                        planeFitTransform3 = doFitPlane((float)0.83639205, (float) 0.53157526, mRgbTimestampGlThread3);
                        //                        // アイテムアバター3
                        //                        planeFitTransform4 = doFitPlane(u, (float) 0.003, mRgbTimestampGlThread4);
                        //                        // アイテムアバター4
                        //                        planeFitTransform5 = doFitPlane(u, (float) 0.005, mRgbTimestampGlThread5);

                        if (planeFitTransform1 != null) {
                            // Place the earth 30 cm above the plane.
                            service.getItemList().enqueue(new Callback<List<User>>() {
                                @Override
                                public void onResponse(final Call<List<User>> call, final Response<List<User>> response) {
                                    List<User> users = response.body();
                                    int i = 1;
                                    for (User user : users) {
                                        Log.v(TAG, user.toString());

                                        String position_x = user.getPosition_x();
                                        String position_y = user.getPosition_y();
                                        String position_z = user.getPosition_z();
                                        String avatar_id = user.getAvatar_id();

                                        float[] planeFitTransform;
                                        synchronized (this) {
                                            Matrix.translateM(planeFitTransform1,  0, parseFloat(position_x), parseFloat(position_y), parseFloat(position_z));
                                            mRenderer.updateEarthTransform(planeFitTransform1, i , Integer.parseInt(avatar_id));
                                        }
                                        i++;
                                    }
                                }

                                @Override
                                public void onFailure(final Call<List<User>> call, final Throwable t) {

                                }
                            });
                            mDepthTPlane = doFitPlane(u, v, mRgbTimestampGlThread1);
                            newMeasuredPoint = getDepthAtTouchPosition(u, v);
                            updateLine(newMeasuredPoint);
                        }
                    }
                }
            }

        }
        return true;
    }

    private float[] doFitPlane(float u, float v, double rgbTimestamp) {
        TangoPointCloudData pointCloud = mPointCloudManager.getLatestPointCloud();

        if (pointCloud == null) {
            return null;
        }

        TangoPoseData openglTdepthPose = TangoSupport.getPoseAtTime(
                pointCloud.timestamp,
                TangoPoseData.COORDINATE_FRAME_START_OF_SERVICE,
                TangoPoseData.COORDINATE_FRAME_CAMERA_DEPTH,
                TangoSupport.TANGO_SUPPORT_ENGINE_OPENGL,
                TangoSupport.TANGO_SUPPORT_ENGINE_TANGO,
                TangoSupport.ROTATION_IGNORED);
        if (openglTdepthPose.statusCode != TangoPoseData.POSE_VALID) {
            Log.w(TAG, "Cant get openglTdepth pose at time "
                    + pointCloud.timestamp);
            return null;
        }

        TangoPoseData openglTcolorPose = TangoSupport.getPoseAtTime(
                rgbTimestamp,
                TangoPoseData.COORDINATE_FRAME_START_OF_SERVICE,
                TangoPoseData.COORDINATE_FRAME_CAMERA_COLOR,
                TangoSupport.TANGO_SUPPORT_ENGINE_OPENGL,
                TangoSupport.TANGO_SUPPORT_ENGINE_TANGO,
                TangoSupport.ROTATION_IGNORED);
        if (openglTcolorPose.statusCode != TangoPoseData.POSE_VALID) {
            Log.w(TAG, "Cannot get openglTcolor pose at time "
                    + rgbTimestamp);
            return null;
        }
        //if (mIsGlRenderer) {
            TangoSupport.IntersectionPointPlaneModelPair pointPlaneModel;
        pointPlaneModel = TangoSupport.fitPlaneModelNearPoint(pointCloud,
                openglTdepthPose.translation, openglTdepthPose.rotation,
                u, v, mDisplayRotation,
                openglTcolorPose.translation, openglTcolorPose.rotation);
        float[] openglUp = new float[]{0, 1, 0, 0};
            float[] openglTplane = matrixFromPointNormalUp(
                    pointPlaneModel.intersectionPoint, pointPlaneModel.planeModel,
                    openglUp);
            return openglTplane;
        //}
    }

    private float[] matrixFromPointNormalUp(double[] point, double[] normal, float[] up) {
        float[] zAxis = new float[]{(float) normal[0], (float) normal[1], (float) normal[2]};
        normalize(zAxis);
        float[] xAxis = crossProduct(up, zAxis);
        normalize(xAxis);
        float[] yAxis = crossProduct(zAxis, xAxis);
        normalize(yAxis);
        float[] m = new float[16];
        Matrix.setIdentityM(m, 0);
        m[0] = xAxis[0];
        m[1] = xAxis[1];
        m[2] = xAxis[2];
        m[4] = yAxis[0];
        m[5] = yAxis[1];
        m[6] = yAxis[2];
        m[8] = zAxis[0];
        m[9] = zAxis[1];
        m[10] = zAxis[2];
        m[12] = (float) point[0];
        m[13] = (float) point[1];
        m[14] = (float) point[2];
        return m;
    }

    /**
     * Normalize a vector.
     */
    private void normalize(float[] v) {
        double norm = Math.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
        v[0] /= norm;
        v[1] /= norm;
        v[2] /= norm;
    }

    /**
     * Cross product between two vectors following the right hand rule.
     */
    private float[] crossProduct(float[] v1, float[] v2) {
        float[] result = new float[3];
        result[0] = v1[1] * v2[2] - v2[1] * v1[2];
        result[1] = v1[2] * v2[0] - v2[2] * v1[0];
        result[2] = v1[0] * v2[1] - v2[0] * v1[1];
        return result;
    }

    private void updateMeshMap() {
        if (mMeshVector != null) {
            Log.d(TAG, "Got mesh");
            for (TangoMesh tangoMesh : mMeshVector) {
                if (tangoMesh != null && tangoMesh.numFaces > 0) {
                    mRenderer.updateMesh(tangoMesh);
                }
            }
            mMeshVector = null;
        }
    }

    /**
     * Use the Tango Support Library with point cloud data to calculate the depth
     * of the point closest to where the user touches the screen. It returns a
     * Vector3 in OpenGL world space.
     */
    private MeasuredPoint getDepthAtTouchPosition(float u, float v) {
        TangoPointCloudData pointCloud = mPointCloudManager.getLatestPointCloud();
        if (pointCloud == null) {
            return null;
        }

        double rgbTimestamp;
        TangoImageBuffer imageBuffer = mCurrentImageBuffer;

        rgbTimestamp = mRgbTimestampGlThread1; // GPU.


        TangoPoseData depthlTcolorPose = TangoSupport.getPoseAtTime(
                rgbTimestamp,
                TangoPoseData.COORDINATE_FRAME_CAMERA_DEPTH,
                TangoPoseData.COORDINATE_FRAME_CAMERA_COLOR,
                TangoSupport.TANGO_SUPPORT_ENGINE_TANGO,
                TangoSupport.TANGO_SUPPORT_ENGINE_TANGO,
                TangoSupport.ROTATION_IGNORED);
        if (depthlTcolorPose.statusCode != TangoPoseData.POSE_VALID) {
            Log.w(TAG, "Could not get color camera transform at time "
                    + rgbTimestamp);
            return null;
        }

        float[] depthPoint;

        depthPoint = TangoSupport.getDepthAtPointNearestNeighbor(
                pointCloud,
                new double[]{0.0, 0.0, 0.0},
                new double[]{0.0, 0.0, 0.0, 1.0},
                u, v,
                mDisplayRotation,
                depthlTcolorPose.translation,
                depthlTcolorPose.rotation);


        if (depthPoint == null) {
            return null;
        }

        return new MeasuredPoint(rgbTimestamp, depthPoint);
    }

    /**
     * Update the oldest line endpoint to the value passed into this function.
     * This will also flag the line for update on the next render pass.
     */
    private synchronized void updateLine(MeasuredPoint newPoint) {
        // 1回目
        if (touchCounter == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("メッセージ");
            builder.setMessage("中心位置はこちらで良いですか？!")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            touchCounter++;
                            // 中心位置が決まったので相手情報を取得して表示

                            final AvatarService service = ServiceGenerator.createService(AvatarService.class);

                            if (UserType.equals("sender")) {
                                service.getReceiverData().enqueue(new Callback<List<User>>() {
                                    @Override
                                    public void onResponse(final Call<List<User>> call, final Response<List<User>> response) {
                                        List<User> users = response.body();
                                        for (User user : users) {
                                            Log.v(TAG, user.toString());

                                            String position_x = user.getPosition_x();
                                            String position_y = user.getPosition_y();
                                            String position_z = user.getPosition_z();
                                            String avatar_id = user.getAvatar_id();

                                            float[] planeFitTransform;
                                            synchronized (this) {
                                                if(mRgbTimestampGlThread1 != 0) {
                                                    planeFitTransform = doFitPlane(0.5f, 0.5f, mRgbTimestampGlThread1);
                                                    Matrix.translateM(planeFitTransform, 0, parseFloat(position_x), parseFloat(position_y), parseFloat(position_z));
                                                    mRenderer.updateEarthTransform(planeFitTransform, 5, Integer.parseInt(avatar_id));
                                                    Log.e("type 5", "OK");
                                                }
                                            }

                                            /*service.positionRegister(position_x, position_y, position_z, avatar_id).enqueue(new Callback<ResponseBody>() {
                                                @Override
                                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                                }

                                                @Override
                                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                                    Log.v(TAG, "onFailure   ////  " + t.getMessage());
                                                }
                                            });*/
                                        }
                                    }

                                    @Override
                                    public void onFailure(final Call<List<User>> call, final Throwable t) {

                                    }
                                });
                            } else {
                                service.getSenderData().enqueue(new Callback<List<User>>() {
                                    @Override
                                    public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                                        List<User> users = response.body();
                                        for (User user : users) {
                                            Log.v(TAG, user.toString());

                                            String position_x = user.getPosition_x();
                                            String position_y = user.getPosition_y();
                                            String position_z = user.getPosition_z();
                                            String avatar_id = user.getAvatar_id();

                                            float[] planeFitTransform;
                                            synchronized (this) {
                                                if(mRgbTimestampGlThread1 != 0) {
                                                    planeFitTransform = doFitPlane(0.5f, 0.5f, mRgbTimestampGlThread1);
                                                    Matrix.translateM(planeFitTransform, 0, parseFloat(position_x), parseFloat(position_y), parseFloat(position_z));
                                                    mRenderer.updateEarthTransform(planeFitTransform, 5, Integer.parseInt(avatar_id));
                                                }
                                                Log.e("type 5", "OK");
                                            }



                                            /*service.positionRegister(position_x, position_y, position_z, avatar_id).enqueue(new Callback<ResponseBody>() {
                                                @Override
                                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                                }

                                                @Override
                                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                                    Log.v(TAG, "onFailure   ////  " + t.getMessage());
                                                }
                                            });*/
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<List<User>> call, Throwable t) {
                                        Log.v(TAG, "onFailure   ////  " + t.getMessage());
                                    }
                                });
                            }

                            return;
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    });

            if (mPointSwitch == 0) {
                mMeasuredPoints[0] = newPoint;
                mMeasuredPoints[2] = newPoint;
                mMeasuredPoints[3] = newPoint;
                AlertDialog dialog = builder.create();
                dialog.show();
            }
            return;
        } else {
            double rgbTimestamp;
            TangoImageBuffer imageBuffer = mCurrentImageBuffer;

            rgbTimestamp = mRgbTimestampGlThread1; // GPU.

            TangoPoseData depthlTcolorPose = TangoSupport.getPoseAtTime(
                    rgbTimestamp,
                    TangoPoseData.COORDINATE_FRAME_CAMERA_DEPTH,
                    TangoPoseData.COORDINATE_FRAME_CAMERA_COLOR,
                    TangoSupport.TANGO_SUPPORT_ENGINE_TANGO,
                    TangoSupport.TANGO_SUPPORT_ENGINE_TANGO,
                    TangoSupport.ROTATION_IGNORED);
            newPoint.mDepthTPoint[0] = (float) depthlTcolorPose.translation[0];
            newPoint.mDepthTPoint[1] = (float) depthlTcolorPose.translation[1];
            newPoint.mDepthTPoint[2] = (float) depthlTcolorPose.translation[2];

            mMeasuredPoints[1] = newPoint;

            final AvatarService service = ServiceGenerator.createService(AvatarService.class);
            // 送信
            if (UserType.equals("sender")) {
                service.sendPosition(String.valueOf(newPoint.mDepthTPoint[0]), String.valueOf(newPoint.mDepthTPoint[1]), String.valueOf(newPoint.mDepthTPoint[2]), String.valueOf(1)).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.d("submit","submit");
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.v(TAG, "onFailure   ////  " + t.getMessage());
                    }
                });

                service.getReceiverData().enqueue(new Callback<List<User>>() {
                    @Override
                    public void onResponse(final Call<List<User>> call, final Response<List<User>> response) {
                        List<User> users = response.body();
                        for (User user : users) {
                            Log.v(TAG, user.toString());

                            String position_x = user.getPosition_x();
                            String position_y = user.getPosition_y();
                            String position_z = user.getPosition_z();
                            String avatar_id = user.getAvatar_id();

                            float[] planeFitTransform;
                            synchronized (this) {
                                if(mRgbTimestampGlThread1 != 0){
                                    planeFitTransform = doFitPlane(0.5f, 0.5f, mRgbTimestampGlThread1);
                                    Matrix.translateM(planeFitTransform, 0, parseFloat(position_x), parseFloat(position_y), parseFloat(position_z));
                                    mRenderer.updateEarthTransform(planeFitTransform, 5, Integer.parseInt(avatar_id));
                                }
                            }

                            Log.e("type 5", "OK");
                        }
                    }

                    @Override
                    public void onFailure(final Call<List<User>> call, final Throwable t) {

                    }
                });
            } else {
                service.positionRegister(String.valueOf(newPoint.mDepthTPoint[0]), String.valueOf(newPoint.mDepthTPoint[1]), String.valueOf(newPoint.mDepthTPoint[2]), String.valueOf(1)).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.v(TAG, "onFailure   ////  " + t.getMessage());
                    }
                });

                service.getSenderData().enqueue(new Callback<List<User>>() {
                    @Override
                    public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                        List<User> users = response.body();
                        for (User user : users) {
                            Log.v(TAG, user.toString());

                            String position_x = user.getPosition_x();
                            String position_y = user.getPosition_y();
                            String position_z = user.getPosition_z();
                            String avatar_id = user.getAvatar_id();

                            float[] planeFitTransform;
                            synchronized (this) {

                                if(mRgbTimestampGlThread1 != 0){
                                    planeFitTransform = doFitPlane(0.5f, 0.5f, mRgbTimestampGlThread1);

                                    Matrix.translateM(planeFitTransform, 0, parseFloat(position_x), parseFloat(position_y), parseFloat(position_z));
                                    mRenderer.updateEarthTransform(planeFitTransform, 5, Integer.parseInt(avatar_id));
                                    Log.e("type 5", "OK");
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<List<User>> call, Throwable t) {
                        Log.v(TAG, "onFailure   ////  " + t.getMessage());
                    }
                });
            }
            // 2回目
            //mPointSwitch = !mPointSwitch;
            //mMeasuredPoints[1] = newPoint;
            return;
        }
    }

    /*
     * Remove all the points from the Scene.
     */
    private synchronized void clearLine() {
        mMeasuredPoints[0] = null;
        mMeasuredPoints[1] = null;
        mMeasuredPoints[2] = null;
        mMeasuredPoints[3] = null;
        mPointSwitch = 0;
        mPointRenderer.setLine(null, null);
    }


    /**
     * Set the color camera background texture rotation and save the display rotation.
     */
    @SuppressLint("WrongConstant")
    private void setDisplayRotation() {
        Display display = getWindowManager().getDefaultDisplay();
        mDisplayRotation = display.getRotation();

        // We also need to update the camera texture UV coordinates. This must be run in the OpenGL
        // thread.
        mSurfaceView.queueEvent(new Runnable() {
            @Override
            public void run() {
                if (mIsConnected) {
                    if (mIsGlRenderer) {
                        //mPointRenderer.updateColorCameraTextureUvGlThread(mDisplayRotation);
                    }
                    mRenderer.updateColorCameraTextureUv(mDisplayRotation);
                }
            }
        });
    }

    /**
     * Check to see if we have the necessary permissions for this app; ask for them if we don't.
     *
     * @return True if we have the necessary permissions, false if we don't.
     */
    private boolean checkAndRequestPermissions() {
        if (!hasCameraPermission()) {
            requestCameraPermission();
            return false;
        }
        return true;
    }

    /**
     * Check to see if we have the necessary permissions for this app.
     */
    private boolean hasCameraPermission() {
        return ContextCompat.checkSelfPermission(this, CAMERA_PERMISSION) ==
                PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Request the necessary permissions for this app.
     */
    private void requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, CAMERA_PERMISSION)) {
            showRequestPermissionRationale();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{CAMERA_PERMISSION},
                    CAMERA_PERMISSION_CODE);
        }
    }

    /**
     * If the user has declined the permission before, we have to explain that the app needs this
     * permission.
     */
    private void showRequestPermissionRationale() {
        final AlertDialog dialog = new AlertDialog.Builder(this)
                .setMessage("Java Point to point Example requires camera permission")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ActivityCompat.requestPermissions(PointToPointActivity.this,
                                new String[]{CAMERA_PERMISSION}, CAMERA_PERMISSION_CODE);
                    }
                })
                .create();
        dialog.show();
    }

    /**
     * Display toast on UI thread.
     *
     * @param resId The resource id of the string resource to use. Can be formatted text.
     */
    private void showsToastAndFinishOnUiThread(final int resId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(PointToPointActivity.this,
                        getString(resId), Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }

    /**
     * Result for requesting camera permission.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (hasCameraPermission()) {
            bindTangoService();
        } else {
            Toast.makeText(this, "Java Point to point Example requires camera permission",
                    Toast.LENGTH_LONG).show();
        }
    }

    public void onClickTextButton(View view) {
    }

    public void onClickHeartButton(View view) {
    }
}
