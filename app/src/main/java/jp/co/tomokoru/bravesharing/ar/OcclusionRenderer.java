/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.co.tomokoru.bravesharing.ar;

import com.google.atap.tango.mesh.TangoMesh;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import java.util.HashMap;
import java.util.Stack;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import jp.co.tomokoru.bravesharing.R;
import jp.co.tomokoru.bravesharing.meshing.GridIndex;
import jp.co.tomokoru.bravesharing.meshing.MeshSegment;
import jp.co.tomokoru.bravesharing.opengl.DepthTexture;
import jp.co.tomokoru.bravesharing.opengl.OpenGlCameraPreview;
import jp.co.tomokoru.bravesharing.opengl.OpenGlSphere;

import org.rajawali3d.Object3D;
import org.rajawali3d.math.vector.Vector3;

/**
 * An OpenGL renderer that renders the Tango RGB camera texture as a background and an earth sphere.
 * It also renders a depth texture to occlude the sphere if it is behind an object.
 */
public class OcclusionRenderer implements GLSurfaceView.Renderer {

    /**
     * A small callback to allow the caller to introduce application-specific code to be executed
     * in the OpenGL thread.
     */
    public interface RenderCallback {
        void preRender();
    }

    @SuppressLint("UseSparseArrays")
    private final HashMap<GridIndex, MeshSegment> mMeshMap = new HashMap<GridIndex, MeshSegment>();

    private RenderCallback mRenderCallback;
    private DepthTexture mDepthTexture1;
    private DepthTexture mDepthTexture2;
    private DepthTexture mDepthTexture3;
    private DepthTexture mDepthTexture4;
    private DepthTexture mDepthTexture5;
    private DepthTexture mDepthTexture6;
    private DepthTexture mDepthTexture7;
    private OpenGlCameraPreview mOpenGlCameraPreview;
    private OpenGlSphere mOpenGlSphere1;
    private OpenGlSphere mOpenGlSphere2;
    private OpenGlSphere mOpenGlSphere3;
    private OpenGlSphere mOpenGlSphere4;
    private OpenGlSphere mOpenGlSphere5;
    private OpenGlSphere mOpenGlSphere6;
    private OpenGlSphere mOpenGlSphere7;
    private Context mContext;
    private boolean mProjectionMatrixConfigured;

    private float[] mViewMatrix = new float[16];
    private float[] mProjectionMatrix = new float[16];
    private float[] mVPMatrix1 = new float[16];

    private Object3D mLine;
    private Object3D mLineSender;
    private Stack<Vector3> mPoints;
    private Stack<Vector3> mPointsSender;
    private boolean mLineUpdated = false;

    private  boolean flag = false;

    public OcclusionRenderer(Context context, RenderCallback callback) {
        mContext = context;
        mRenderCallback = callback;
        mOpenGlCameraPreview = new OpenGlCameraPreview();

        mOpenGlSphere1 = new OpenGlSphere();
        mOpenGlSphere2 = new OpenGlSphere();
        mOpenGlSphere3 = new OpenGlSphere();
        mOpenGlSphere4 = new OpenGlSphere();
        mOpenGlSphere5 = new OpenGlSphere();

        float[] worldTsphere1 = new float[16];
        Matrix.setIdentityM(worldTsphere1, 0);
        Matrix.translateM(worldTsphere1, 0, 0, 0, -2);
        mOpenGlSphere1.setModelMatrix(worldTsphere1);

        float[] worldTsphere2 = new float[16];
        Matrix.setIdentityM(worldTsphere2, 0);
        Matrix.translateM(worldTsphere2, 0, 0.1f, 0, -2);
        mOpenGlSphere2.setModelMatrix(worldTsphere2);

        float[] worldTsphere3 = new float[16];
        Matrix.setIdentityM(worldTsphere3, 0);
        Matrix.translateM(worldTsphere3, 0, 0.2f, 0, -2);
        mOpenGlSphere3.setModelMatrix(worldTsphere3);

        float[] worldTsphere4 = new float[16];
        Matrix.setIdentityM(worldTsphere4, 0);
        Matrix.translateM(worldTsphere4, 0, 0.3f, 0, -2);
        mOpenGlSphere4.setModelMatrix(worldTsphere4);

        float[] worldTsphere5 = new float[16];
        Matrix.setIdentityM(worldTsphere5, 0);
        Matrix.translateM(worldTsphere5, 0, 0.4f, 0, -2);
        mOpenGlSphere5.setModelMatrix(worldTsphere5);

        mDepthTexture1 = new DepthTexture();
        float[] worldTmesh1 = new float[16];
        Matrix.setIdentityM(worldTmesh1, 0);
        Matrix.rotateM(worldTmesh1, 0, -90, 1, 0, 0);
        mDepthTexture1.setModelMatrix(worldTmesh1);

        mDepthTexture2 = new DepthTexture();
        float[] worldTmesh2 = new float[16];
        Matrix.setIdentityM(worldTmesh2, 0);
        Matrix.rotateM(worldTmesh2, 0, -90, 1, 0, 0);
        mDepthTexture2.setModelMatrix(worldTmesh2);

        mDepthTexture3 = new DepthTexture();
        float[] worldTmesh3 = new float[16];
        Matrix.setIdentityM(worldTmesh3, 0);
        Matrix.rotateM(worldTmesh3, 0, -90, 1, 0, 0);
        mDepthTexture3.setModelMatrix(worldTmesh3);

        mDepthTexture4 = new DepthTexture();
        float[] worldTmesh4 = new float[16];
        Matrix.setIdentityM(worldTmesh4, 0);
        Matrix.rotateM(worldTmesh4, 0, -90, 1, 0, 0);
        mDepthTexture4.setModelMatrix(worldTmesh4);

        mDepthTexture5 = new DepthTexture();
        float[] worldTmesh5 = new float[16];
        Matrix.setIdentityM(worldTmesh5, 0);
        Matrix.rotateM(worldTmesh5, 0, -90, 1, 0, 0);
        mDepthTexture5.setModelMatrix(worldTmesh5);
    }

    @Override
    public void onSurfaceCreated(GL10 Object, EGLConfig eglConfig) {
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        GLES20.glEnable(GLES20.GL_CULL_FACE);
        GLES20.glCullFace(GLES20.GL_BACK);
        GLES20.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        GLES20.glDepthMask(true);
        mOpenGlCameraPreview.setUpProgramAndBuffers();
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        Bitmap earthBitmap1 = android.graphics.BitmapFactory.decodeResource(mContext.getResources(),
                R.drawable.avatar1, options);

        Bitmap earthBitmap2 = android.graphics.BitmapFactory.decodeResource(mContext.getResources(),
                R.drawable.avatar2, options);

        Bitmap earthBitmap3 = android.graphics.BitmapFactory.decodeResource(mContext.getResources(),
                R.drawable.avatar3, options);

        Bitmap earthBitmap4 = android.graphics.BitmapFactory.decodeResource(mContext.getResources(),
                R.drawable.avatar4, options);

        Bitmap earthBitmap5 = android.graphics.BitmapFactory.decodeResource(mContext.getResources(),
                R.drawable.avatar5, options);

        mOpenGlSphere1.setUpProgramAndBuffers(earthBitmap1, mContext);
        mOpenGlSphere2.setUpProgramAndBuffers(earthBitmap2, mContext);
        mOpenGlSphere3.setUpProgramAndBuffers(earthBitmap3, mContext);
        mOpenGlSphere4.setUpProgramAndBuffers(earthBitmap4, mContext);
        mOpenGlSphere5.setUpProgramAndBuffers(earthBitmap5, mContext);
        mDepthTexture1.resetDepthTexture();
        mDepthTexture2.resetDepthTexture();
        mDepthTexture3.resetDepthTexture();
        mDepthTexture4.resetDepthTexture();
        mDepthTexture5.resetDepthTexture();
        mMeshMap.clear();
    }

    /**
     * Update background texture's UV coordinates when device orientation is changed (i.e., change
     * between landscape and portrait mode).
     */
    public void updateColorCameraTextureUv(int rotation) {
        mOpenGlCameraPreview.updateTextureUv(rotation);
        mProjectionMatrixConfigured = false;
    }

    @Override
    public void onSurfaceChanged(GL10 gl10, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
        mDepthTexture1.setTextureSize(width, height);
        mDepthTexture2.setTextureSize(width, height);
        mDepthTexture3.setTextureSize(width, height);
        mDepthTexture4.setTextureSize(width, height);
        mDepthTexture5.setTextureSize(width, height);
        mOpenGlSphere1.setDepthTextureSize(width, height);
        mOpenGlSphere2.setDepthTextureSize(width, height);
        mOpenGlSphere3.setDepthTextureSize(width, height);
        mOpenGlSphere4.setDepthTextureSize(width, height);
        mOpenGlSphere5.setDepthTextureSize(width, height);
        mProjectionMatrixConfigured = false;
    }

    @Override
    public void onDrawFrame(GL10 gl10) {

        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        mRenderCallback.preRender();

        GLES20.glDepthMask(false);
        mOpenGlCameraPreview.drawAsBackground();
        GLES20.glDepthMask(true);

        updateVPMatrix();

        mDepthTexture1.renderDepthTexture(mMeshMap, mVPMatrix1);
        int depthTexture1 = mDepthTexture1.getDepthTextureId();

        mDepthTexture2.renderDepthTexture(mMeshMap, mVPMatrix1);
        int depthTexture2 = mDepthTexture2.getDepthTextureId();

        mDepthTexture3.renderDepthTexture(mMeshMap, mVPMatrix1);
        int depthTexture3 = mDepthTexture3.getDepthTextureId();

        mDepthTexture4.renderDepthTexture(mMeshMap, mVPMatrix1);
        int depthTexture4 = mDepthTexture4.getDepthTextureId();

        mDepthTexture5.renderDepthTexture(mMeshMap, mVPMatrix1);
        int depthTexture5 = mDepthTexture5.getDepthTextureId();

        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        if(flag == true){
            mOpenGlSphere1.drawSphere(mVPMatrix1, depthTexture1);
            mOpenGlSphere2.drawSphere(mVPMatrix1, depthTexture2);
            mOpenGlSphere3.drawSphere(mVPMatrix1, depthTexture3);
            mOpenGlSphere4.drawSphere(mVPMatrix1, depthTexture4);
            mOpenGlSphere5.drawSphere(mVPMatrix1, depthTexture5);
        }
    }

    /**
     * Set the Projection matrix matching the Tango RGB camera in order to be able to do
     * augmented reality.
     */
    public void setProjectionMatrix(float[] matrixFloats, float nearPlane, float farPlane) {
        mProjectionMatrix = matrixFloats;
        mOpenGlSphere1.configureCamera(nearPlane, farPlane);
        mOpenGlSphere2.configureCamera(nearPlane, farPlane);
        mOpenGlSphere3.configureCamera(nearPlane, farPlane);
        mOpenGlSphere4.configureCamera(nearPlane, farPlane);
        mOpenGlSphere5.configureCamera(nearPlane, farPlane);
        mProjectionMatrixConfigured = true;
    }

    /**
     * Update the View matrix matching the pose of the Tango RGB camera.
     *
     * @param ssTcamera The transform from RGB camera to Start of Service.
     */
    public void updateViewMatrix(float[] ssTcamera) {
        float[] viewMatrix = new float[16];
        Matrix.invertM(viewMatrix, 0, ssTcamera, 0);
        mViewMatrix = viewMatrix;
    }

    /**
     * Composes the view and projection matrices into a single VP matrix.
     */
    private void updateVPMatrix() {
        Matrix.setIdentityM(mVPMatrix1, 0);
        Matrix.multiplyMM(mVPMatrix1, 0, mProjectionMatrix, 0, mViewMatrix, 0);
    }

    /**
     * Update the mesh segments given a new TangoMesh.
     */
    public void updateMesh(TangoMesh tangoMesh) {
        GridIndex key = new GridIndex(tangoMesh.index);
        if (!mMeshMap.containsKey(key)) {
            mMeshMap.put(key, new MeshSegment());
        }
        MeshSegment mesh = mMeshMap.get(key);
        mesh.update(tangoMesh);
        mMeshMap.put(key, mesh);
    }

    /**
     * It returns the ID currently assigned to the texture where the Tango color camera contents
     * should be rendered.
     * NOTE: This must be called from the OpenGL render thread; it is not thread safe.
     */
    public int getTextureId() {
        return mOpenGlCameraPreview == null ? -1 : mOpenGlCameraPreview.getTextureId();
    }

    /**
     * Updates the earth model matrix.
     */
    public void updateEarthTransform(float[] openGlTearthl,int type, int avatar_id) {
        Bitmap bitmap = null;
        final BitmapFactory.Options options = new BitmapFactory.Options();
        /*if(avatar_id == 1) {
            bitmap = android.graphics.BitmapFactory.decodeResource(mContext.getResources(),
                    R.drawable.avatar1, options);
        } else if(avatar_id == 2) {
            bitmap = android.graphics.BitmapFactory.decodeResource(mContext.getResources(),
                    R.drawable.avatar2, options);
        } else if(avatar_id == 3) {
            bitmap = android.graphics.BitmapFactory.decodeResource(mContext.getResources(),
                    R.drawable.avatar3, options);
        } else if(avatar_id == 4) {
            bitmap = android.graphics.BitmapFactory.decodeResource(mContext.getResources(),
                    R.drawable.avatar4, options);
        } else if(avatar_id == 5) {
            bitmap = android.graphics.BitmapFactory.decodeResource(mContext.getResources(),
                    R.drawable.avatar5, options);
        } else if(avatar_id == 6) {
            bitmap = android.graphics.BitmapFactory.decodeResource(mContext.getResources(),
                    R.drawable.avatar6, options);
        } else if(avatar_id == 7) {
            bitmap = android.graphics.BitmapFactory.decodeResource(mContext.getResources(),
                    R.drawable.avatar7, options);
        }*/
        if (type == 1) {
            mOpenGlSphere1.setModelMatrix(openGlTearthl);
            //mOpenGlSphere1.setUpProgramAndBuffers(bitmap, mContext);
            //mDepthTexture1.resetDepthTexture();
        } else if (type == 2) {
            mOpenGlSphere2.setModelMatrix(openGlTearthl);
            //mOpenGlSphere2.setUpProgramAndBuffers(bitmap, mContext);
            //mDepthTexture2.resetDepthTexture();
        } else if (type == 3) {
            mOpenGlSphere3.setModelMatrix(openGlTearthl);
            //mOpenGlSphere3.setUpProgramAndBuffers(bitmap, mContext);
            //mDepthTexture3.resetDepthTexture();
        } else if (type == 4) {
            mOpenGlSphere4.setModelMatrix(openGlTearthl);
            //mOpenGlSphere4.setUpProgramAndBuffers(bitmap, mContext);
            //mDepthTexture4.resetDepthTexture();
        } else if (type == 5) {
            mOpenGlSphere5.setModelMatrix(openGlTearthl);
            //mOpenGlSphere5.setUpProgramAndBuffers(bitmap, mContext);
            //mDepthTexture5.resetDepthTexture();
        }

        flag = true;
    }

    public boolean isProjectionMatrixConfigured() {
        return mProjectionMatrixConfigured;
    }

    public synchronized void setLine(Stack<Vector3> points, Stack<Vector3> pointsSender) {
        mPoints = points;
        mPointsSender = pointsSender;
        mLineUpdated = true;
    }
}
