package jp.co.tomokoru.bravesharing;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import jp.co.tomokoru.bravesharing.service.AvatarService;
import jp.co.tomokoru.bravesharing.service.ServiceGenerator;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChooseAvatarActivity extends AppCompatActivity {

    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_avatar);

        mActivity = this;
    }

    public void onClickImage(View view) {
        String tag = view.getTag().toString();
        final String avatarId = tag.substring(tag.length() - 1, tag.length());

        ServiceGenerator.createService(AvatarService.class).uploadAvatar(avatarId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(final Call<ResponseBody> call, final Response<ResponseBody> response) {
                Intent intent = new Intent();
                intent.putExtra("avatarId", avatarId);
                setResult(RESULT_OK, intent);
                mActivity.finish();
            }

            @Override
            public void onFailure(final Call<ResponseBody> call, final Throwable t) {
                Toast.makeText(mActivity, "エラーが発生しました。時間をあけ、再度トライして下さい。", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
