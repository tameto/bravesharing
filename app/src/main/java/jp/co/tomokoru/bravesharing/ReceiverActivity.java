package jp.co.tomokoru.bravesharing;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareMediaContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.widget.ShareDialog;
import com.onesignal.OneSignal;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ReceiverActivity extends AppCompatActivity {

    private static final String TAG = ReceiverActivity.class.getSimpleName();

    CallbackManager mCallbackManager;

    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receiver);

        mActivity = this;
        mCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(final LoginResult loginResult) {
                Log.v(TAG, String.valueOf(loginResult.getAccessToken() == null));
                if (loginResult.getAccessToken() != null) {
                    Log.v(TAG, loginResult.getAccessToken().getToken());

                    Bitmap bitmap = BitmapFactory.decodeResource(mActivity.getResources(), android.R.drawable.stat_sys_warning);

                    SharePhoto photo = new SharePhoto.Builder()
                            .setBitmap(bitmap)
                            .build();

                    ShareContent shareContent = new ShareMediaContent.Builder()
                            .addMedium(photo)
                            .setShareHashtag(new ShareHashtag.Builder().setHashtag("#sharing-yuuki").build())
                            .build();

                    ShareDialog shareDialog = new ShareDialog(mActivity);
                    shareDialog.show(shareContent, ShareDialog.Mode.AUTOMATIC);
                }
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(final FacebookException error) {
                Log.v(TAG, "onError");
            }
        });
    }

    public void onClickPlay(View view) {
    }

    public void onClickAvatar(View view) {
    }

    public void onClickSOS(View view) {
        //            String playerId = "8fcccc40-645d-4b43-b858-23d29b6fce3a"; // Fujitsu
        String playerId = "7ae9c689-d117-4595-98e5-f8e5fef5bdf7"; // Pixel

        SharedPreferences prefer = getSharedPreferences("devil", MODE_PRIVATE);
        String ssoMessage = prefer.getString("sos_message", "ほんのちょっとすつだけみんなの勇気を分けてくれ！");

        Map<String, String> contents = new HashMap<>();
        contents.put("ja", ssoMessage);
        contents.put("en", ssoMessage);

        Map map = new HashMap<>();
        map.put("contents", contents);
        map.put("include_player_ids", Arrays.asList(playerId));

        OneSignal.postNotification(new JSONObject(map), null);

        new AlertDialog.Builder(this)
                .setTitle("Facebook投稿")
                .setMessage("あなたのSOSをFacebookにも投稿しますか？")
                .setPositiveButton("はい", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        LoginManager.getInstance().logInWithReadPermissions(mActivity, Arrays.asList("email public_profile"));
                    }
                })
                .setNegativeButton("いいえ", null)
                .create()
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
